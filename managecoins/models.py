from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash
import datetime

db = SQLAlchemy()

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True)
    email = db.Column(db.String(40))
    password = db.Column(db.String(66))
    balance=db.Column(db.Integer);
    created_date = db.Column(db.DateTime, default= datetime.datetime.now)

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = self.__create_pasword(password)

    def __create_pasword(self, password):
        return generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password, password)

class Moneda(db.Model):
    __tablename__ = 'moneda'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(80), unique=True, nullable=False, primary_key=True)
    valor = db.Column(db.Integer, unique=True, nullable=False)

    def __repr__(self):
        return "<Nombre: {}>".format(self.title)


class Transaction(db.Model):
    __tablename__ = 'transaction'

    id = db.Column(db.Integer, primary_key=True)
    user_id_from = db.Column(db.Integer,  db.ForeignKey('users.id') )
    user_id_to = db.Column(db.Integer,db.ForeignKey('users.id'))
    mount = db.Column(db.Integer)
    coin_id = db.Column(db.Text(),db.ForeignKey('moneda.id'))
    created_date = db.Column(db.DateTime, default= datetime.datetime.now)

    moneda = db.relationship(Moneda, backref="Transaction")
    user = db.relationship(User, foreign_keys=[user_id_from])
    user_to = db.relationship(User, foreign_keys=[user_id_to])

