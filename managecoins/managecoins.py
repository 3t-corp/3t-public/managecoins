from flask import Flask, render_template ,request ,redirect, url_for
from flask_bootstrap import Bootstrap
from config import DevelopmentConfig

from models import db
from models import Moneda
from models import User
from models import Transaction

from functions import manageBalance, userExist ,crearMoneda


app = Flask(__name__)
app.config.from_object(DevelopmentConfig)
bootstrap = Bootstrap(app)

@app.route('/logout')
def logout():
 return redirect(url_for('index'))


@app.route('/')
def index():
 return render_template('index.html')


@app.route('/account', methods=["GET", "POST"])
def login():
    error = None
    if request.method == "POST":
        users = User.query.all()
        for user in users:
            print user.username
            if userExist(request.form['user'],request.form['password']):
                user = request.form['user']
                return redirect(url_for("account_status", username=user))
    else:
        error = "Invalid Username or Password"
    return render_template('login.html', error=error)


@app.route('/account/<username>')
def account_status(username):
    user = User.query.filter_by(username=username).first()
    transactions = Transaction.query.filter_by(user_id_from=user.id).all()
    users=User.query.all()
    TipoMoneda=Moneda.query.all()
    return render_template('account.html',user=user,transactions=transactions,users=users,TipoMoneda=TipoMoneda)


@app.route('/account/<username>/transfer', methods=['GET', 'POST'])
def transfer_moneyz(username):
    monedas=Moneda.query.all()
    if request.method == 'GET':
        return render_template('transfer.html', username=username,monedas=monedas)
    else:
        toUser = request.form['to']
        amount = int(request.form['amount'])
        coin = int(request.form['coin'])
        if toUser == username:
            error = "no se puede transferir entre el mismo usuario"
            return render_template('transfer.html', error=error)
        if not userExist(toUser):
            error = "no existe '{}'".format(toUser)
            return render_template('transfer.html', error=error)
    manageBalance(username,toUser,amount,coin)
    return redirect(url_for("account_status", username=username))

@app.route("/crear/<username>/", methods=["GET", "POST"])
def crear_money(username):
    if request.method == 'GET':
        render_template("crearmoneda.html", monedas=Moneda.query.all(),username=username)
    if request.form:
        crearMoneda(request.form.get("nombre"),request.form.get("valor"))
    return render_template("crearmoneda.html",username=username, monedas=Moneda.query.all())

@app.route('/faq')
def faq():
    return render_template('faq.html')

if __name__ == '__main__':
    db.init_app(app)

    with app.app_context():
        db.create_all()
    app.run(debug=True)
 
