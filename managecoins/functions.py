from models import db
from models import Moneda
from models import User
from models import Transaction
def manageBalance(user_from,user_to,mount,moneda):
    if moneda >=1:
        valorconvertido=moneda*mount    
    else:
        valorconvertido=abs(mount/moneda)
    user_from=User.query.filter_by(username=user_from).first()
    user_from.balance-=valorconvertido
    db.session.commit()
    user_to=User.query.filter_by(username=user_to).first()
    user_to.balance+=valorconvertido
    db.session.commit()
    m=Moneda.query.filter_by(valor=moneda).first()
    db.session.add(Transaction(user_id_from=user_from.id,user_id_to=user_to.id,mount=mount,coin_id=m.id))
    db.session.commit()


def userExist(user,pwd=""):
    users = User.query.all()
    exist=False
    for u in users:
        if pwd:
            if user == u.username and pwd == u.password:
                exist = True;
        else:
            print u.username
            if user == u.username:
                exist = True;
    if exist:
        return True
    else:
        return False

def crearMoneda(nombre,valor):
    db.session.add(Moneda(nombre=nombre,valor=valor))
    db.session.commit()

